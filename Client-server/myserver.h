#ifndef MYSERVER_H
#define MYSERVER_H

#include <QTcpServer>
#include <QTcpSocket>
#include <QFile>
#include "string"
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlTableModel>
#include "QDebug"
#include <QtSql/QSqlError>
#include <qsqldriverplugin.h>
#include <qstringlist.h>
#include <QtSql>
#include <QCoreApplication>
#include <QVariant>
#include <QDebug>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QTextStream>
#include <QDataStream>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstdlib>
#include <clocale>
#include <QDataStream>
#include <openssl/evp.h>
#include <openssl/aes.h>
#include <openssl/rsa.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>



#define BUFSIZE 1024


using namespace std;

class myserver: public QObject
{
    Q_OBJECT
public:
    explicit myserver(QObject *parent = 0); // конструктор

    QByteArray Data;
    bool mess;
    void con(QSqlDatabase db){
        db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName("C:/sqlite/access.db");
        db.open();
        if(!db.open())
        {
            qDebug() << "Error = " << db.lastError().text();
        }
        else
        {
            qDebug() << "DataBase is open!";
        }
    }
    string random_string( size_t length )
    {
        auto randchar = []() -> char
        {
            const char charset[] =
            "0123456789"
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            "abcdefghijklmnopqrstuvwxyz";
            const size_t max_index = (sizeof(charset) - 1);
            return charset[ rand() % max_index ];
        };
        string str(length,0);
        generate_n( str.begin(), length, randchar );
        return str;
    }
    string random_char( size_t length )
    {
        auto randchar = []() -> char
        {
        const char charset[] =
        "0123456789";
        const size_t max_index = (sizeof(charset) - 1);
        return charset[ rand() % max_index ];
        };
    string str(length,0);
    generate_n( str.begin(), length, randchar );
    return str;
    }
    QString sendDB()
    {
        QString basa = "";
        QSqlQuery query;
        query.exec("SELECT * FROM access");
        QSqlRecord rec = query.record();
        const int idIndex = rec.indexOf("id");
        const int nameIndex = rec.indexOf("name");
        const int yearIndex = rec.indexOf("year");
        const int profIndex = rec.indexOf("prof");
        const int phoneIndex = rec.indexOf("phone");
        const int loginIndex = rec.indexOf("login");
        const int passIndex = rec.indexOf("pass");
        const int accessIndex = rec.indexOf("access");
        while(query.next())
        {
            basa += query.value(idIndex).toString() + ":" +  query.value(nameIndex).toString() + ":" + query.value(yearIndex).toString() + ":" +   query.value(profIndex).toString() +
                    ":" + query.value(phoneIndex).toString() + ":" +  query.value(loginIndex).toString() + ":" +  query.value(passIndex).toString() + ":" +  query.value(accessIndex).toString() + "$";
        }
        return basa;

    }
//    int do_crypt(FILE *in, FILE *out, int do_encrypt){
//        unsigned char inbuf[BUFSIZE], outbuf[BUFSIZE + EVP_MAX_BLOCK_LENGTH]; //EVP_MAX_BLOCK_LENGTH = 128 бит
//        int inlen, outlen;

//        unsigned char key[] = "0123456789abcdeF0123456789abcdeF"; //256 бит
//        unsigned char iv[] = "1234567887654321"; //128 бит

//        EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();

//        EVP_CipherInit_ex(ctx, EVP_aes_256_cbc(), NULL, NULL, NULL, do_encrypt);
//        OPENSSL_assert(EVP_CIPHER_CTX_key_length(ctx) == 32);
//        OPENSSL_assert(EVP_CIPHER_CTX_iv_length(ctx) == 16);
//        EVP_CipherInit_ex(ctx, NULL, NULL, key, iv, do_encrypt);

//        for(;;){
//            inlen = fread(inbuf, 1, BUFSIZE, in);
//            printf("%d B, In:\n", inlen);
//            if (inlen <= 0) break;
//            if(!EVP_CipherUpdate(ctx, outbuf, &outlen, inbuf, inlen)){
//                EVP_CIPHER_CTX_free(ctx);
//                return 0;
//            }
//            fwrite(outbuf, 1, outlen, out);
//            printf("  %d B, OutU:\n", outlen);
//            //printCharsAsHex(outbuf, outlen);
//        }

//        if(!EVP_CipherFinal_ex(ctx, outbuf, &outlen)){
//            EVP_CIPHER_CTX_free(ctx);
//            return 0;
//        }
//        fwrite(outbuf, 1, outlen, out);
//        printf("  %d B, OutF:\n", outlen);
//        //printCharsAsHex(outbuf, outlen);

//        EVP_CIPHER_CTX_free(ctx);
//        return 1;
//    }
    void encrypt()
    {
//        FILE *encode_file = fopen("./1.txt", "rb");
//        FILE *decode_file = fopen("./2.txt", "wb");

        QFile file("./1.txt");
        file.open(QIODevice::WriteOnly|QFile::Text);
        file.write("./2.txt");
        file.close();

        //do_crypt(encode_file, decode_file, 1); // 0 - decrypt, 1 - encrypt
//        fclose(encode_file);
//        fclose(decode_file);

    }
    void decrypt(){
       // FILE *encode_file = fopen("./2.txt", "rb");
       // FILE *decode_file = fopen("./1.txt", "wb");
        QFile file("./2.txt");
        file.open(QIODevice::WriteOnly|QFile::Text);
        file.write("./1.txt");
        file.close();
        //do_crypt(encode_file, decode_file, 0); // 0 - decrypt, 1 - encrypt
      //  fclose(encode_file);
      //  fclose(decode_file);
    }

    // do_crypt(encode_file, decode_file, 1); // 0 - decrypt расшифровывать, 1 - encrypt шифровать


public slots:
    void readyRead();
    void incomingConnection();
    void stateChanged(QAbstractSocket::SocketState state);

private:
    QMap<int, QTcpSocket*> SWorAuth;
    QMap<int, QTcpSocket*> SAdminAuth;
    QMap<int, QTcpSocket*> SClients;
    QTcpServer * mTcpServer;
    QList<QTcpSocket *> AdminSoc; // получатели данных
    QList<QTcpSocket *> WorSoc; // полвещ данных
    QList<QTcpSocket *> ClientSoc;



};

#endif // MYSERVER_H
