#include "QtSql/QSqlDatabase"
#include "QSqlQuery"
#include "myserver.h"

myserver::myserver(QObject *parent) :
    QObject(parent)
{
    setlocale(LC_ALL, "Rus");
    mTcpServer = new QTcpServer(this);
    qDebug() << "server listen = " << mTcpServer->listen(QHostAddress::Any, 6666);
    QSqlDatabase db;
    con(db);
    QSqlQuery query = QSqlQuery(db);
    connect(mTcpServer, SIGNAL(newConnection()), this, SLOT(incomingConnection())); // подключаем сигнал "новое подключение" к нашему обработчику подключений
}



void myserver::incomingConnection()
{

   QTcpSocket * socket = mTcpServer->nextPendingConnection(); // получаем сокет нового входящего подключения
   int id = (int)socket->socketDescriptor();
   SClients[id] = socket;
   connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(stateChanged(QAbstractSocket::SocketState))); // делаем обработчик изменения статуса сокета
   connect(socket,SIGNAL(readyRead()),this,SLOT(readyRead()));

    qDebug()<< socket <<" Client connected";
    ClientSoc << socket;
}

void myserver::readyRead()
{
    QObject * object = QObject::sender(); // далее и ниже до цикла идет преобразования "отправителя сигнала" в сокет, дабы извлечь данные
    if (!object)
        return;

    QTcpSocket * socket = static_cast<QTcpSocket *>(object);

    QString Message;


    Message = socket->readAll();
    qDebug()<<"Data: "<<Message;

    QSqlQuery query;
    QString id, Name, Year, Prof, Phone, Login, Pass, access;

    int k = 0;
    string token;
    istringstream streamS(Message.toStdString());
    while(getline(streamS, token, ':')) k++;
    qDebug() << k;

    QTcpSocket *temp = (QTcpSocket*)sender();
    int Id = (int)temp->socketDescriptor();

    if(Message == "showDB")
    {
        QByteArray answerBd = "";
        answerBd = sendDB().toUtf8().data();
        socket->write(answerBd);
//        QFile file("./1.txt");
//        file.open(QIODevice::WriteOnly|QFile::Text);
//        file.write(answerBd);
//        file.close();
//        encrypt(); // Шифровать для отправки
//          //decrypt(); // Расшифровывать

//        file.setFileName("./2.txt");
//        if (file.open(QIODevice::ReadOnly|QFile::Text))
//        {
//            QByteArray fromFile = file.readAll();
//            qDebug() << fromFile;

//            socket->write(fromFile);
//        }
//        file.close();



       // socket->write(cryptsting(answerBd,1));

    }else if(k == 1)
    {
        if ((SWorAuth[Id] == SClients[Id]) || (SAdminAuth[Id] == SClients[Id])) qDebug() << query.exec(Message);
    }
    if(k == 2)
    {
        k = 0;
        istringstream streamS(Message.toStdString());
        while(getline(streamS, token, ':'))
        {
            if(k == 0) Login = QString::fromUtf8(token.c_str());
            if(k == 1) Pass = QString::fromUtf8(token.c_str());
            k++;
        }

        query.exec("SELECT * FROM access");
        while(query.next())
        {
            if (Login == query.value(5).toString() and Pass == query.value(6).toString())
            {
                id = query.value(0).toString();
                access = query.value(7).toString();
                break;
            }
        }
        QString text = id + ":" + access;
        if (access == "admin")
        {
            SAdminAuth[Id] = SClients[Id];
            AdminSoc << socket;
        }
        else if(access == "artist")
        {
            SWorAuth[Id] = SClients[Id];
            WorSoc << socket;
        }
        qDebug() << text;
        socket->write(text.toUtf8().data());
    }

    if((SWorAuth[Id] == SClients[Id]) or (SAdminAuth[Id] == SClients[Id]))
    {
        if(k == 5)
        {
            k = 0;
            istringstream streamS(Message.toStdString());
            while(getline(streamS, token, ':'))
            {
                if(k == 0) Name = QString::fromUtf8(token.c_str());
                if(k == 1) Year = QString::fromUtf8(token.c_str());
                if(k == 2) Prof = QString::fromUtf8(token.c_str());
                if(k == 3) Phone = QString::fromUtf8(token.c_str());
                if(k == 4) access = QString::fromUtf8(token.c_str());
                Login = QString::fromUtf8(random_string(5).c_str());
                Pass = QString::fromUtf8(random_char(5).c_str());
                k++;
            }
            QString Mes = "INSERT INTO access (id, name, year, prof, phone, login, pass, access) VALUES (NULL, '"+Name+"', '"+Year+"', '"+Prof+"', '"+Phone+"', '"+Login+"', '"+Pass+"', '"+access+"')";
            qDebug() << query.exec(Mes);
        }
        if(k == 7 or k == 8)
        {
            k = 0;
            istringstream streamS(Message.toStdString());
            while(getline(streamS, token, ':'))
            {
                if(k == 0) id = QString::fromUtf8(token.c_str());
                if(k == 1) Name = QString::fromUtf8(token.c_str());
                if(k == 2) Year = QString::fromUtf8(token.c_str());
                if(k == 3) Prof = QString::fromUtf8(token.c_str());
                if(k == 4) Phone = QString::fromUtf8(token.c_str());
                if(k == 5) Login = QString::fromUtf8(token.c_str());
                if(k == 6) Pass = QString::fromUtf8(token.c_str());
                if(k == 7) access = QString::fromUtf8(token.c_str());
                k++;
            }
            if(id != "")
            {
                if(Name != "") qDebug() << query.exec("UPDATE `access` SET `name` = '"+Name+"' WHERE `access`.`id` = "+id);
                if(Year != "") qDebug() << query.exec("UPDATE `access` SET `year` = '"+Year+"' WHERE `access`.`id` = "+id);
                if(Prof != "") qDebug() << query.exec("UPDATE `access` SET `prof` = '"+Prof+"' WHERE `access`.`id` = "+id);
                if(Phone != "") qDebug() << query.exec("UPDATE `access` SET `phone` = '"+Phone+"' WHERE `access`.`id` = "+id);
                if(Login != "") qDebug() << query.exec("UPDATE `access` SET `login` = '"+Login+"' WHERE `access`.`id` = "+id);
                if(Pass != "") qDebug() << query.exec("UPDATE `access` SET `pass` = '"+Pass+"' WHERE `access`.`id` = "+id);
                if(access != "") qDebug() << query.exec("UPDATE `access` SET `access` = '"+access+"' WHERE `access`.`id` = "+id);
            }
        }
    }



    //    QFile file;
    //    file.setFileName("D:/Desktop/Client-server/news.txt");
    //    if (file.open(QIODevice::ReadOnly|QFile::Text))
    //    {
    //        QByteArray fromFile = file.readAll();

    //        socket->write(fromFile);
    //        socket->waitForBytesWritten(500);
    //    }
    //    file.close();

}

void myserver::stateChanged(QAbstractSocket::SocketState state) // обработчик статуса, нужен для контроля за "вещающим"
{
    QObject * object = QObject::sender();
    if (!object)
        return;
    QTcpSocket * socket = static_cast<QTcpSocket *>(object);
    qDebug() << socket << " disconnect";
}
