#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTextCodec>
#include <QGraphicsView>


  MainWindow::MainWindow(QWidget *parent) :
      QMainWindow(parent),
      ui(new Ui::MainWindow)
{
    ui->setupUi(this);
//    QPixmap bkgnd("..//1.jpg");
//    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
//    QPalette palette;
//    palette.setBrush(QPalette::Background, bkgnd);
//    this->setPalette(palette);
    QPixmap pixmap("..//1.jpg");
    //ui->pic->setPixmap(pixmap);


    socket = new QTcpSocket(this);
    connect(socket, SIGNAL(readyRead()),this,SLOT(sockReady()));
    connect(socket, SIGNAL(disconnected()),this,SLOT(sockDisc()));
    ui->pushButtonBD->hide();
    ui->pushButton_2->hide();
    ui->pushButton_3->hide();
    ui->pushButton->hide();
    ui->groupBoxAdd->hide();
    ui->groupBoxEd->hide();
    ui->id->hide();
    ui->label_6->hide();
    ui->Delete->hide();
    ui->Artist_4->setChecked(false);
    ui->Admin_3->setChecked(false);
    ui->tableView->hide();

//    ui->verticalLayout_3->hide();
//    ui->groupBox->hide();
//    ui->groupBoxEd->hide();
//    ui->groupBoxButton->hide();
//    ui->tableView->hide();
//    ui->groupBoxCom->hide();
//    ui->up->hide();
//    ui->down->hide();
//    ui->textEdit->hide();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::sockDisc()
{
    socket->deleteLater();
}

void MainWindow::sockReady()
{

}

void MainWindow::on_LoginB_clicked()
{
    socket->connectToHost("127.0.0.1",6666);
    QString Login = ui->Login->text();
    string login = Login.toStdString();
    QString Password = ui->Pass->text();
    string password = Password.toStdString();

    QString text = Login + ":" + Password;

    if(!login.empty() and !password.empty())
    {
        socket->write(text.toUtf8().data());


        socket->waitForReadyRead(500);
        QString text = socket->readAll();

        int k = 0;
        string token;
        istringstream streamS(text.toStdString());
        while(getline(streamS, token, ':'))
        {
            if(k == 0) id = QString::fromUtf8(token.c_str());
            if(k == 1) status = QString::fromUtf8(token.c_str());
            k++;
        }

//        ui->text->append(access);

        if(status == "admin")
        {
            ui->groupBoxLog->hide();
            ui->tableView->show();
            ui->pushButtonBD->show();
            ui->pushButton_2->show();
            ui->pushButton_3->show();
            ui->pushButton->show();

//            ui->groupBoxButton->show();
//            ui->up->show();
//            ui->down->show();
        }
        else if(status == "artist")
        {
            ui->groupBoxLog->hide();
            ui->tableView->show();
            ui->pushButtonBD->show();

//            ui->groupBoxEd->show();
//            ui->up->show();
//            ui->down->show();
//            ui->id_2->setText(id);
//            ui->id_2->setDisabled(1);
//            ui->Admin_3->hide();
//            ui->Artist_3->hide();
        }

    }
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->groupBoxAdd->show();
    ui->groupBoxEd->hide();
    ui->id->hide();
    ui->label_6->hide();
    ui->Delete->hide();
}

void MainWindow::on_pushButton_3_clicked()
{
    ui->groupBoxEd->show();
    ui->groupBoxAdd->hide();
    ui->id->hide();
    ui->label_6->hide();
    ui->Delete->hide();
//    ui->groupBoxAdd->hide();
//    ui->groupBoxCom->hide();
//    ui->groupBoxButton->hide();
}

void MainWindow::on_pushButton_clicked()
{
    ui->id->show();
    ui->label_6->show();
    ui->Delete->show();
    ui->groupBoxEd->hide();
    ui->groupBoxAdd->hide();
}

//void MainWindow::on_pushButtonCom_clicked()
//{
//    QString answer;
//    QString text = ui->lineEdit->text();
//    if(!text.toStdString().empty())
//    {
//        socket->write(text.toUtf8().data());
//        socket->waitForReadyRead(500);
//        answer = socket->readAll();
//        if(answer == "true")
//        {
//            // ОКНО ЧТО ВСЕ ОК
//        }
//        else{
//            // ОКНО ЧТО ВЫПОЛНИТЬ КОМАНДУ НЕ УДАЛОСЬ
//        }
//    }
//}

void MainWindow::on_Add_clicked()
{
    QString Name = ui->nam->text();
    QString Year = ui->Year->text();
    QString Prof = ui->Prof->text();
    QString Phone = ui->Phone->text();
    QString access;
    if(ui->Artist->isChecked())
       {
           access = "artist";
       }
       else
       {
           access = "admin";
       }
    /* Можно сделать чтоб отправлял только команду и исключения у клиента
       или же отправлять список и парсить на сервере */
    QString text = Name + ":" + Year + ":" + Prof + ":" + Phone + ":" + access;
    socket->write(text.toUtf8().data()); // Отправляем на сервер для парсинга


    ui->nam->clear();
    ui->Year->clear();
    ui->Prof->clear();
    ui->Phone->clear();
    ui->groupBoxAdd->hide();
}

void MainWindow::on_pushButtonBD_clicked()
{
    QString text = "showDB";
    socket->write(text.toUtf8().data());
    socket->waitForReadyRead(1000);
    QString answer = socket->readAll();

    qDebug() << answer;

//    QByteArray answerBd;
//    answerBd = answer.toUtf8().data();
//    QFile file("./2.txt");
//    file.open(QIODevice::WriteOnly|QFile::Text);
//    file.write(answerBd);
//    file.close();
//        //encrypt(); // Шифровать для отправки
//    decrypt(); // Расшифровывать

//    file.setFileName("./1.txt");
//    if (file.open(QIODevice::ReadOnly|QFile::Text))
//    {
//        answer = file.readAll();
//        ui->textEdit->append(answer);

//    }
//    file.close();




    QStandardItemModel *model = new QStandardItemModel;
    QStandardItem *item;

    QStringList horizontalHeader;



    horizontalHeader.append("id");
    horizontalHeader.append("name");
    horizontalHeader.append("year");
    horizontalHeader.append("prof");
    horizontalHeader.append("phone");
    horizontalHeader.append("login");
    horizontalHeader.append("pass");
    horizontalHeader.append("access");

    QStringList verticalHeader;

    model->setHorizontalHeaderLabels(horizontalHeader);
    model->setVerticalHeaderLabels(verticalHeader);

    int k = 0, j = 0;
    string token;
    QString Name, Year, Prof, Phone, Login, Pass, access, id;


    istringstream streamS(answer.toStdString());
    while(getline(streamS, token, '$'))
    {
       text = QString::fromUtf8(token.c_str());

       k = 0;
       istringstream streamS2(text.toStdString());
       while(getline(streamS2, token, ':'))
       {
           if(k == 0) id = QString::fromUtf8(token.c_str());
           if(k == 1) Name = QString::fromUtf8(token.c_str());
           if(k == 2) Year = QString::fromUtf8(token.c_str());
           if(k == 3) Prof = QString::fromUtf8(token.c_str());
           if(k == 4) Phone = QString::fromUtf8(token.c_str());
           if(k == 5) Login = QString::fromUtf8(token.c_str());
           if(k == 6) Pass = QString::fromUtf8(token.c_str());
           if(k == 7) access = QString::fromUtf8(token.c_str());
           k++;
       }
       item = new QStandardItem(QString(id));
       model->setItem(j, 0, item);

       item = new QStandardItem(QString(Name));
       model->setItem(j, 1, item);

       item = new QStandardItem(QString(Year));
       model->setItem(j, 2, item);

       item = new QStandardItem(QString(Prof));
       model->setItem(j, 3, item);

       item = new QStandardItem(QString(Phone));
       model->setItem(j, 4, item);

       item = new QStandardItem(QString(Login));
       model->setItem(j, 5, item);

       item = new QStandardItem(QString(Pass));
       model->setItem(j, 6, item);

       item = new QStandardItem(QString(access));
       model->setItem(j, 7, item);
       j++;
    }

    for (int c = 0; c < ui->tableView->horizontalHeader()->count(); ++c)
    {
    ui->tableView->horizontalHeader()->setSectionResizeMode(c, QHeaderView::Stretch);
    }

    QFont font = ui->tableView->font();
    font.setPixelSize(size);
    ui->tableView->setFont(font);
    ui->tableView->horizontalHeader()->setFont( font );
    ui->tableView->verticalHeader()->setFont( font );
    ui->tableView->setModel(model);
    ui->tableView->resizeRowsToContents();
    ui->tableView->resizeColumnsToContents();


    // ПЕРЕДАТЬ МОДЕЛЬ С СЕРВЕРА
//    QSqlQuery query;
//    QSqlQueryModel *M = new QSqlQueryModel();
//    M->setQuery(query);
//    ui->tableView->setModel(M);
}

void MainWindow::on_Delete_clicked()
{
    //ТОЛЬКО ОТПРАВЛЯТЬ ЗАПРОС
    QString text = "DELETE FROM `access` WHERE `access`.`id` = " + ui->id->text();
    socket->write(text.toUtf8().data());
    ui->id->clear();
    ui->id->hide();
    ui->label_6->hide();
    ui->Delete->hide();

}

void MainWindow::on_Edit_clicked()
{

    QString Id = ui->id_2->text();
    QString Name = ui->Name_3->text();
    QString Year = ui->Year_3->text();
    QString Prof = ui->Prof_3->text();
    QString Phone = ui->Phone_3->text();
    QString Login = ui->login->text();
    QString Pass = ui->pass->text();
    QString access;
    if(ui->Artist_4->isChecked()) access = "artist";
    else if(ui->Admin_3->isChecked()) access = "admin";
    else access = "";

    QString text = Id + ":" + Name + ":" + Year + ":" + Prof + ":" + Phone + ":" + Login + ":" + Pass + ":" + access;
    socket->write(text.toUtf8().data());

    ui->Name_3->clear();
    ui->Year_3->clear();
    ui->Prof_3->clear();
    ui->Phone_3->clear();
    ui->login->clear();
    ui->pass->clear();



    ui->Artist_4->setCheckable(false);
    ui->Admin_3->setCheckable(false);

    ui->Artist_4->setCheckable(true);
    ui->Admin_3->setCheckable(true);


    if(status == "artist")
    {

    }
    else if (status == "admin")
    {
        ui->id_2->clear();
        //ui->groupBoxButton->show();
        ui->groupBoxEd->hide();
    }
    ui->groupBoxEd->hide();
}


